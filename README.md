#KeyLogger

Key logger module


## Install

```sh
$ npm install --save @adwatch/keylogger
```


## Usage

```js
import KeyLogger from '@adwatch/keylogger';
// or
var KeyLogger = require('@adwatch/keylogger/build');
```


## API


####onFilter(event, validType)

Check symbol from client

#####validType Table
| data-f | Description |
|:------:|:-----------:|
|oC|Only Cyrillic Symbols and spaces|
|oL|Only Latin Symbols and spaces|
|oN|Only Numbers Symbols|
|oE|Only Symbols resolved in email address |

```js
$('.elem').on('keypress', function(e){
	let validType = $(this).data('f');

	if(keyLogger.onFilter(e, validType)){
		//forbidden symbol
		return false;
	}else{
		//OK
	}
});
```


####filterBlur(val, validType)

Check symbol from client

```js
this.$groupInputs.on('blur', (e)=>{
	var elem = e.target,
		val = elem.value,
		validType = elem.getAttribute('data-f');

	if(val){
		if(keyLogger.filterBlur(val, validType)){

	            //Bad symbols
	            val = '';
	            return false;
		}
	}
});
```


####logXss(val)

Catch xss on clint side

```js
this.$groupInputs.on('blur', (e)=>{
	var elem = e.target,
		val = elem.value,
		validType = elem.getAttribute('data-f');

	if(val){
		if(keyLogger.logXss(val)){

			//Bad symbols
			val = '';
			return false;
		}
	}
});
```


## License

MIT ©
